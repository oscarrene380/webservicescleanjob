<?php
require_once('Error.php');
require_once '../Services/CuentaService.php';
$CuentaService = new CuentaService();

if($_SERVER['REQUEST_METHOD'] === 'POST')
{
	try 
	{
		if(!isset($_POST['recuperar']))
		{
			if(!isset($_POST['user']) || !isset($_POST['password']))
			{
				throw new Exception("Todos los campos son requeridos");
			}
			
			$user = $_POST["user"];
			$newPassword = $_POST["password"];

			$rawdata = array();
			$rawdata=$CuentaService->ResetPassword($user, $newPassword);
		   	if($rawdata == 1)
		   	{
		   		$Status = new Status("Cuenta","200","La contraseña se cambio con exito");
				echo json_encode($Status);
		   	}
		   	else
		   	{
		   		$Status = new Status("Cuenta","401","No se actualizo su contraseña");
				echo json_encode($Status);
		   	}
		}
		else
		{
			if(!isset($_POST['user'])){
				throw new Exception("Todos los campos son necesarios - restore");
			}

			$user = $_POST['user'];
			$rawdata = array();
			$rawdata = $CuentaService->RecuperarCuenta($user);
		   	if($rawdata == 1)
		   	{
		   		$Status = new Status("Cuenta","200","Se cambio la clave");
				echo json_encode($Status);
		   	}
		   	else
		   	{
		   		$Status = new Status("Cuenta","401","Error cambiando clave");
				echo json_encode($Status);
		   	}
		}

	}
	catch (Exception $e) 
	{
       $Status = new Status("Cuenta","401",$e->getMessage());
	   echo json_encode($Status);
    }
}

if($_SERVER['REQUEST_METHOD'] === 'GET')
{
	try
	{
		if(!isset($_GET['user']) || !isset($_GET['password']) )
			throw new Exception("Todos los campos son requeridos");

		$rawdata = $CuentaService->Login($_GET['user'],$_GET['password']);
		echo json_encode($rawdata);
	}
	catch(Exception $e)
	{
		$Status = new Status("Cuenta","401",$e->getMessage());
	   	echo json_encode($Status);
	}
}