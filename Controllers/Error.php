<?php
class Status 
{
    private $name;
    private $code;
    private $msg;

    public function __construct($ErrorName, $ErrorCode, $ErrorMSG)
    {
        $this->name = $ErrorName;
        $this->code = $ErrorCode;
        $this->msgs = $ErrorMSG;
    }
    public function getCode()
    {
        return $this->codes;
    }
    public function getName()
    {
        return $this->names;
    }
    public function getMsg()
    {
        return $this->msgs;
    }
	public function expose() 
    {
        return get_object_vars($this);
	}
}