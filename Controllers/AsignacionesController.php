<?php
require_once('Error.php');
require_once '../Services/AsignacionesService.php';
$AsignacionesService = new AsignacionesService();

if($_SERVER['REQUEST_METHOD'] === 'PUT')
{


}

if($_SERVER['REQUEST_METHOD'] === 'POST')
{
	try
	{
		validarAsiganacionesPost();
		$idUserAsignado = $_POST['idUserAsignado'];
		$idUser = $_POST['idUser'];
		$FechaAsignado = $_POST['FechaAsignado'];
		$idTipoAsig = $_POST['idTipoAsig'];
		$titulo = $_POST['titulo'];
		$Descripcion = $_POST['Descripcion'];
		$idlab = $_POST['idlab'];

		$rawdata = array();
		$rawdata = $Asignaciones->CrearAsignacion($idUserAsignado,$idUser,$FechaAsignado,$idTipoAsig,$titulo,$Descripcion,$idlab);

		switch ($rawdata) 
		{
			case 0:
				$Status = new Status("Asignaciones","200","Orden de limpieza creada con exito");
				echo json_encode($Status);
				break;
			case 1:
				$Status = new Status("Asignaciones","200","El ordenanza no pertenece al laboratorio seleccionado");
				echo json_encode($Status);
				break;
			case 2:
				$Status = new Status("Asignaciones","200","El ordenanza ya tiene asignaciones en este rango de tiempo");
				echo json_encode($Status);
				break;
			case 3:
				$Status = new Status("Asignaciones","200","El ordenanza seleccionado no esta disponible en este turno");
				echo json_encode($Status);
				break;
			case 4:
				$Status = new Status("Asignaciones","200","El ordenanza seleccionado no esta disponible en este turno");
				echo json_encode($Status);
				break;
			default:
				$Status = new Status("Asignaciones","401","Respuesta no identificada");
				echo json_encode($Status);
				break;
		}
	}
	catch(Exception $e)
	{
		$Status = new Status("Asignaciones","401",$e->getMessage());
	   	echo json_encode($Status);
	}
}


if($_SERVER['REQUEST_METHOD'] === 'DELETE')
{


}

if($_SERVER['REQUEST_METHOD'] === 'GET')
{
	try
	{
		$rawdata = array();
		if(isset($_GET['buscar']))
		{
			$idlab = $_POST['idlab'];
			$idUserAsignado = $_POST['idUserAsignado'];
			$idsEstados = $_POST['idsEstados'];
			$idUsercreo = $_POST['idUsercreo'];

			$rawdata = $Asignaciones->ListarOrdenes($idlab,$idUserAsignado,$idsEstados,$idUsercreo);
			echo json_encode($rawdata);
		}
		else if(isset(isset($_GET['detalle']))) // aqui solo recibira el id de la orden
		{

		}
		else
		{	
			throw new Exception("Debe especificar una accion");
			
		}
	}
	catch(Exception $e)
	{
		$Status = new Status("Asignaciones","401",$e->getMessage());
	   	echo json_encode($Status);
	}
}

function validarAsiganacionesPost()
{ 
	if(!isset($_POST['idUserAsignado']) || !isset($_POST['idUser']) || !isset($_POST['FechaAsignado']) || !isset($_POST['idTipoAsig']) || !isset($_POST['titulo']) || 
		!isset($_POST['Descripcion']) || !isset($_POST['idlab']) ){
		throw new Exception("Todos los campos son requeridos");
	}

 	if(empty($_POST['idUserAsignado']) || empty($_POST['idUser']) || empty($_POST['FechaAsignado']) || empty($_POST['idTipoAsig']) || empty($_POST['titulo']) 
 		|| empty($_POST['Descripcion']) || empty($_POST['idlab']) )
 	{
 		throw new Exception("Todos los campos son requeridos");
 	}
}