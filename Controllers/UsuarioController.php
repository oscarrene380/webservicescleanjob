<?php
require_once('Error.php');
require_once '../Services/UsuarioService.php';
$UserService = new UsuarioService();

if($_SERVER['REQUEST_METHOD'] === 'POST')
{
	try 
	{
		if(!isset($_POST['editar']))
		{	
			validarUserPost();
			$idUser =$_POST["idUser"];
			$nombre = $_POST["nombre"];
			$apellido =$_POST["apellido"];
			$edad=$_POST["edad"];
			$genero=$_POST["genero"];
			$idTurno=$_POST["idTurno"];
			$idTipo= $_POST["idTipo"];
			$idLab = $_POST["idLab"];
			
			$rawdata = array();
			$rawdata=$UserService->InsertUsers($idUser,$nombre,$apellido,$edad,$genero,$idTurno,$idTipo,$idLab);
		   
		    if($rawdata ==1)
		    {
				$Status = new Status("Users","200","Usuario ingresado correctamente");
				echo json_encode($Status);	
			}
			else
			{
				$Status = new Status("Users","401","Error al ingresar un usuario XD");
				echo json_encode($Status);
			}
		}
		else
		{	
			validarUserPost();
			$idUser =$_POST["idUser"];
			$nombre = $_POST["nombre"];
			$apellido =$_POST["apellido"];
			$edad=$_POST["edad"];
			$genero=$_POST["genero"];
			$idTurno=$_POST["idTurno"];
			$idTipo= $_POST["idTipo"];
			$idLab = $_POST["idLab"];

			$rawdata = array();
			$rawdata=$UserService->UpdateUser($idUser,$nombre,$apellido,$edad,$genero,$idTurno,$idTipo,$idLab);

			if($rawdata ==1)
		    {
				$Status = new Status("Users","200","Usuario actualizado correctamente");
				echo json_encode($Status);	
			}
			else
			{
				$Status = new Status("Users","401","Error al actualizar un usuario");
				echo json_encode($Status);
			}
		}
	}
	catch (Exception $e) 
	{
		 
       $Status = new Status("Users","401",$e->getMessage());
	   echo json_encode($Status);
    }
}


if($_SERVER['REQUEST_METHOD'] === 'PUT')
{
	
	try 
	{
		validarUserPut();
		parse_str(file_get_contents("php://input"), $_PUT);

		$idUser =$_PUT["idUser"];
		$nombre = $_PUT["nombre"];
		$apellido =$_PUT["apellido"];
		$edad=$_PUT["edad"];
		$genero=$_PUT["genero"];
		$pass=$_PUT["pass"];
		$idTurno=$_PUT["idTurno"];
		$idTipo= $_PUT["idTipo"];
		$idLab = $_PUT["idLab"];
		
		$rawdata = array();
		$rawdata=$UserService->UpdateUsers($idUser,$nombre,$apellido,$edad,$genero,$pass,$idTurno,$idTipo,$idLab);
	   
	    $Status = new Status("Users","200","Usuario actualizado correctamente");
		echo '<pre>'.$Status.'</pre>';
 
	}
	catch (Exception $e) 
	{
		 
       $Status = new Status("Users","401",$e->getMessage());
	   echo json_encode($Status);
    }
	
}

if($_SERVER['REQUEST_METHOD'] === 'DELETE')
{
	try 
	{
		parse_str(file_get_contents("php://input"), $_DELETE);
		
		if((isset($_DELETE["idUser"])) ==false || empty($_DELETE["idUser"]))
			throw new Exception("Debe enviar el identificador del usuario");
		
		$idUser =$_DELETE["idUser"];
		
		$rawdata = array();
		$rawdata=$UserService->DeleteUsers($idUser);
		
		if($rawdata ==1)
		{
			$Status = new Status("Users","200","Usuario eliminado correctamente");
			echo json_encode($Status);
		}
		else
		{	
			$Status = new Status("Users","401","Error al eliminar un usuario");
			echo json_encode($Status);
		}
	}
	catch (Exception $e) 
	{
       $Status = new Status("Users","401",$e->getMessage());
	   echo json_encode($Status);
    }
}

if($_SERVER['REQUEST_METHOD'] === 'GET')
{
	try
	{
		if(isset($_GET['buscar']))
		{
			if(!isset($_GET['buscar']))	
				throw new Exception("Debe enviar parametros de busqueda");
			
			$rawdata = $UserService->DetalleUser($_GET['buscar']);
			echo json_encode($rawdata);
		}
		else
		{
			$rawdata = $UserService->ListaUsuarios();
			echo json_encode($rawdata);
		}
	}
	catch(Exception $e)
	{
		$Status = new Status("Usuario","401",$e->getMessage());
	   	echo json_encode($Status);
	}
}

//Valida los parametros de los  usuarios al momento de hacer las peticiones 
function validarUserPost()
{
	if((isset($_POST["idUser"]) && isset($_POST["nombre"]) && isset($_POST["apellido"]) && isset($_POST["edad"]) &&
	    isset($_POST["genero"]) && isset($_POST["idTurno"]) && isset($_POST["idTipo"]) && 
	    isset($_POST["idLab"])) == false){
		  throw new Exception("Todos los campos son requeridos");	
	}else if(empty($_POST["idUser"]) || empty($_POST["nombre"]) || empty($_POST["apellido"]) || empty($_POST["edad"]) ||
	    empty($_POST["genero"]) || empty($_POST["idTurno"]) || empty($_POST["idTipo"]) || empty($_POST["idLab"]) ){
		   throw new Exception("Todos los campos son requeridos");
	}
}

//valida el usuario del metodo put
function validarUserPut(){
	
	parse_str(file_get_contents("php://input"), $_PUT);
	
	if((isset($_PUT["idUser"]) && isset($_PUT["nombre"]) && isset($_PUT["apellido"]) && isset($_PUT["edad"]) &&
	    isset($_PUT["genero"]) && isset($_PUT["pass"]) && isset($_PUT["idTurno"]) && isset($_PUT["idTipo"]) && isset($_PUT["idLab"])) == false)
	{
	
		  throw new Exception("Todos los campos son requeridos");
		
	}else if(empty($_PUT["idUser"]) || empty($_PUT["nombre"]) || empty($_PUT["apellido"]) || empty($_PUT["edad"]) ||
	    empty($_PUT["genero"]) || empty($_PUT["pass"]) || empty($_PUT["idTurno"]) || empty($_PUT["idTipo"]) || empty($_PUT["idLab"])){

		   throw new Exception("Todos los campos son requeridos");
	}
}