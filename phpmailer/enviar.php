<?php
require_once dirname(__FILE__)."/phpMailer/PHPMailerAutoload.php";
/**
 *  envio de email con credenciales de usuario
 */
class Email
{
	public function sendMail($nombre, $email, $asunto, $accion, $clave)
	{
		// $accion-> 1 = envio de credenciales 2 = recuperacion de contraseña 3 = recuperacion de cuenta
		try
		{
			// Establecer la zona horaria predeterminada a usar. Disponible desde PHP 5.1
			date_default_timezone_set('America/El_Salvador');
			// Imprime algo como: Monday 8th of August 2005 03:12:46 PM
			$fecha = date('l jS \of F Y h:i:s A');
			$mensaje = '';
			$mail = new PHPMailer;
			if($accion == 1)
			{
				$mensaje = $this->bodyMessage($email, $nombre, $clave);
			}
			if($accion == 2)
			{
				$mensaje = $this->bodyMessage2($email, $fecha);
			}
			if($accion == 3)
			{
				$mensaje = $this->bodyMessage3($email, $clave);
			}
			//indico a la clase que use SMTP
			$mail->isSMTP();
			//permite modo debug para ver mensajes de las cosas que van ocurriendo
			//$mail->SMTPDebug = 2;
			//Debo de hacer autenticación SMTP
			$mail->SMTPAuth = true;
			$mail->SMTPSecure = "tls"; //ssl
			//indico el servidor de Gmail para SMTP
			$mail->Host = "smtp.office365.com"; //smtp.gmail.com
			//indico el puerto que usa Gmail
			$mail->Port = 587; //465 - gmail
			//indico un usuario / clave de un usuario de gmail
			$mail->Username = "2500482016@mail.utec.edu.sv";
			$mail->Password = "03121998";
			$mail->CharSet = "UTF-8";
			$mail->From = "2500482016@mail.utec.edu.sv";
			$mail->FromName = "Servicio de limpieza UTEC";
			$mail->Subject = $asunto;
			$mail->addAddress($email, $nombre);
			$mail->MsgHTML($mensaje);

			//echo $mensaje;
			if($mail->Send())
			{
				return true;
			}
			else
			{
				return "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
			}	
		}
		catch (Exception $e) 
		{
		    echo "Message could not be sent. Mailer Error: {$e}";
		}
	}

	public function bodyMessage($email, $nombre, $clave)
	{
		$html = '
		<!DOCTYPE html>
		<html lang="es-SV">
			<head>
				<!-- Required meta tags -->
				<meta charset="utf-8">
				<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
				<title>Cleaning Service UTEC</title>
				<!-- Bootstrap CSS -->
				<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
			</head>
			<body>
				<div class="container-fluid">
					<div class="row mt-3">
						<div class="col-3"></div>
						<div class="col-6 border border-dark">
							<div class="container">
								<div class="row mt-2">
									<div class="col-12 text-center">
										<img src="https://i.imgur.com/uIh2C0e.png" class="rounded" width="100" height="100">
									</div>
								</div>
								<div class="row mt-2">
									<div class="col-12 text-center">
										<h3 class="display-4">Registro de usuario</h3>
									</div>
								</div>
								<div class="row mt-3">
									<div class="col-12 text-center">
										<h4>Hola '.$nombre.'</h4>
									</div>
								</div>
								<div class="row">
									<div class="col-12 text-center">
										<p>Tu registro ha sido exitoso! tus credenciales de acceso se detallan a continuación: </p>
									</div>
								</div>
								<div class="row">
									<div class="col-12 text-center">
										<p><h3>Usuario : '.$email.'</h3></p>
										<p><h3>Contraseña: '.$clave.'</h3></p>
										<p class="text-muted"><small>Recuerda que puedes cambiar tu contraseña en cualquier momento</small></p>
									</div>
								</div>
								<div class="row mt-3">
									<div class="col-12 text-center">
										<p>
											Gracias por confiar en nosotros.
										</p>
										<p>
											Atentamente:<br>
											<strong>Sistemas UTEC El Salvador</strong>
										</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-3"></div>
					</div>
				</div>		
			</body>
		</html>';
		return $html;
	}

	private function bodyMessage2($email, $fecha)
	{
		$html = '
		<!DOCTYPE html>
		<html lang="es-SV">
			<head>
				<!-- Required meta tags -->
				<meta charset="utf-8">
				<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
				<title>Cleaning Service UTEC</title>
				<!-- Bootstrap CSS -->
				<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
			</head>
			<body>
				<div class="container-fluid">
					<div class="row mt-3">
						<div class="col-3"></div>
						<div class="col-6 border border-dark">
							<div class="container">
								<div class="row mt-2">
									<div class="col-12 text-center">
										<img src="https://i.imgur.com/uIh2C0e.png" class="rounded" width="100" height="100">
									</div>
								</div>
								<div class="row mt-2">
									<div class="col-12 text-center">
										<h3 class="display-4">Cambio de contraseña</h3>
									</div>
								</div>
								<div class="row mt-3">
									<div class="col-12 text-center">
										<h3>'.$email.'</h3>
									</div>
								</div>
								<div class="row">
									<div class="col-12 text-center">
										<p>Este correo se ha enviado automaticamente debido a un reciente cambio de contraseña en tu cuenta</p>
										<p>Si fuiste tú puedes hacer caso omiso de este mensaje, de lo contrario comunicate con el equipo de informatica</p>
									</div>
								</div>
								<div class="row">
									<div class="col-12 text-center">
										<p><h4>Evento registrado alrededor de: '.$fecha.'</h4></p>
									</div>
								</div>
								<div class="row mt-3">
									<div class="col-12 text-center">
										<p>
											Gracias por confiar en nosotros.
										</p>
										<p>
											Atentamente:<br>
											<strong>Sistemas UTEC El Salvador</strong>
										</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-3"></div>
					</div>
				</div>		
			</body>
		</html>';
		return $html;
	}

	public function bodyMessage3($email,$clave)
	{
		$html = '
		<!DOCTYPE html>
		<html lang="es-SV">
			<head>
				<!-- Required meta tags -->
				<meta charset="utf-8">
				<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
				<title>Cleaning Service UTEC</title>
				<!-- Bootstrap CSS -->
				<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
			</head>
			<body>
				<div class="container-fluid">
					<div class="row mt-3">
						<div class="col-3"></div>
						<div class="col-6 border border-dark">
							<div class="container">
								<div class="row mt-2">
									<div class="col-12 text-center">
										<img src="https://i.imgur.com/uIh2C0e.png" class="rounded" width="100" height="100">
									</div>
								</div>
								<div class="row mt-2">
									<div class="col-12 text-center">
										<h3 class="display-4">Recuperacion de cuenta</h3>
									</div>
								</div>
								<div class="row mt-3">
									<div class="col-12 text-center">
										<h3>'.$email.'</h3>
									</div>
								</div>
								<div class="row">
									<div class="col-12 text-center">
										<p>Hemos recibido tu solicitud de recuperación de cuenta</p>
										<p>tu nueva clave de acceso se detalla a continuación: </p>
									</div>
								</div>
								<div class="row">
									<div class="col-12 text-center">
										<p><h3>Contraseña: '.$clave.'</h3></p>
										<p class="text-muted"><small>Recuerda que puedes cambiar tu contraseña en cualquier momento</small></p>
									</div>
								</div>
								<div class="row mt-3">
									<div class="col-12 text-center">
										<p>
											Gracias por confiar en nosotros.
										</p>
										<p>
											Atentamente:<br>
											<strong>Sistemas UTEC El Salvador</strong>
										</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-3"></div>
					</div>
				</div>		
			</body>
		</html>';
		return $html;
	}
}