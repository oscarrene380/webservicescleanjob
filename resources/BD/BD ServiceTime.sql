DROP DATABASE IF EXISTS `servicetime`;
CREATE DATABASE  IF NOT EXISTS `servicetime`;
USE `servicetime`;

/*
  TABLA: ciclos
*/
DROP TABLE IF EXISTS `ciclos`;

CREATE TABLE `ciclos` (
  `idCiclo` int(11) NOT NULL AUTO_INCREMENT,
  `ciclo` varchar(200) NOT NULL,
  `fechaInicio` date NOT NULL,
  `fechaFin` date NOT NULL,
  PRIMARY KEY (`idCiclo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

insert into ciclos (ciclo,fechaInicio,fechaFin) values ('01-2020','2020-01-20','2020-06-10'),('02-2020','2020-07-20','2020-12-20'),('03-2020 - interciclo','2020-06-15','2020-07-15');

/*
  TABLA: edificio
*/
DROP TABLE IF EXISTS `edificio`;

CREATE TABLE `edificio` (
  `idEdificio` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  `descripcion` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`idEdificio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

insert into edificio(nombre) values ('Francisco Morazan'),('Benito Juarez'),('Simon Bolivar');

/*
  TABLA: estadoordenlimpieza
*/
DROP TABLE IF EXISTS `estadoordenlimpieza`;

CREATE TABLE `estadoordenlimpieza` (
  `idEstadoOrden` varchar(50) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `SLASegundos` int(11) DEFAULT NULL,
  PRIMARY KEY (`idEstadoOrden`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

insert into estadoordenlimpieza (idEstadoOrden,nombre) values ('PEN','Pendiente'),('EC','En curso'),('EP','En pausa'),('T','Terminada');

/*
  TABLA: horariohabil
*/
DROP TABLE IF EXISTS `horariohabil`;

CREATE TABLE `horariohabil` (
  `idHorario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `offsetHorario` int(11) DEFAULT NULL,
  PRIMARY KEY (`idHorario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

insert into horariohabil (nombre,offsetHorario) 
values ('Dia', 1),('Tarde', 2),('Noche', 3),('Dia completo', 4);
/*
  TABLA: laboratorio
*/
DROP TABLE IF EXISTS `laboratorio`;

CREATE TABLE `laboratorio` (
  `idLab` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  `descripcion` varchar(300) DEFAULT NULL,
  `idHorario` int(11) NOT NULL,
  `idEdificio` int(11) NOT NULL,
  PRIMARY KEY (`idLab`),
  KEY `idHorario` (`idHorario`),
  KEY `idEdificio` (`idEdificio`),
  CONSTRAINT `laboratorio_ibfk_1` FOREIGN KEY (`idHorario`) REFERENCES `horariohabil` (`idHorario`),
  CONSTRAINT `laboratorio_ibfk_2` FOREIGN KEY (`idEdificio`) REFERENCES `edificio` (`idEdificio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

insert into laboratorio (nombre,idHorario,idEdificio) values
('LAB 1',1,1),('LAB 2',1,1),('LAB REDES',1,1),('LAB 6',1,2),('LAB 3 SOTANO',1,2);

/*
  TABLA: tipousuario
*/
DROP TABLE IF EXISTS `tipousuario`;

CREATE TABLE `tipousuario` (
  `idTipo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(128) NOT NULL,
  `descripcion` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`idTipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `tipousuario` (nombre,descripcion) VALUES ('Administrador',NULL),('Encargado',NULL),('Ordenanza',NULL);

/*
  TABLA: turno
*/
DROP TABLE IF EXISTS `turno`;

CREATE TABLE `turno` (
  `idTurno` int(11) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`idTurno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `turno` (Descripcion) VALUES ('Mañana'),('Tarde'),('Noche');

/*
  TABLA: usuario
*/
DROP TABLE IF EXISTS `usuario`;

CREATE TABLE `usuario` (
  `idUser` varchar(80) NOT NULL,
  `nombre` varchar(128) NOT NULL,
  `apellido` varchar(128) NOT NULL,
  `edad` int(11) DEFAULT NULL,
  `genero` char(1) DEFAULT NULL,
  `pass` varchar(256) DEFAULT NULL,
  `fechaCreacion` date NOT NULL,
  `idTurno` int(11) NOT NULL,
  `idTipo` int(11) NOT NULL,
  PRIMARY KEY (`idUser`),
  KEY `usuario_ibfk_1` (`idTurno`),
  KEY `usuario_ibfk_2` (`idTipo`),
  CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`idTurno`) REFERENCES `turno` (`idTurno`),
  CONSTRAINT `usuario_ibfk_2` FOREIGN KEY (`idTipo`) REFERENCES `tipousuario` (`idTipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*
  TABLA: tipoasignacion
*/
DROP TABLE IF EXISTS `tipoasignacion`;

CREATE TABLE `tipoasignacion` (
  `idTipoAsignacion` int(11) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(90) NOT NULL,
  PRIMARY KEY (`idTipoAsignacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

insert into tipoasignacion(Descripcion) values ('Dia'),('Mes'),('Ciclo');

/*
  TABLA: ordenlimpiezalab
*/
DROP TABLE IF EXISTS `ordenlimpiezalab`;

CREATE TABLE `ordenlimpiezalab` (
  `idOrden` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(200) NOT NULL,
  `descripcion` varchar(1000) DEFAULT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaInicioProgreso` datetime DEFAULT NULL,
  `fechaFinalizo` datetime DEFAULT NULL,
  `fechaInicio` datetime NOT NULL,
  `fechaProyectadaFin` datetime NOT NULL,
  `idEstadoOrden` varchar(50) NOT NULL,
  `idLab` int(11) NOT NULL,
  `idUserAsignado` varchar(80) NOT NULL,
  `idTipoAsignacion` int(11) DEFAULT NULL,
  `idUserCreo` varchar(80) NOT NULL,
  PRIMARY KEY (`idOrden`),
  KEY `idEstadoOrden` (`idEstadoOrden`),
  KEY `idLab` (`idLab`),
  KEY `idUserAsignado` (`idUserAsignado`),
  KEY `idTipoAsignacion` (`idTipoAsignacion`),
  KEY `idUserCreo` (`idUserCreo`),
  CONSTRAINT `ordenlimpiezalab_ibfk_1` FOREIGN KEY (`idEstadoOrden`) REFERENCES `estadoordenlimpieza` (`idEstadoOrden`),
  CONSTRAINT `ordenlimpiezalab_ibfk_2` FOREIGN KEY (`idLab`) REFERENCES `laboratorio` (`idLab`),
  CONSTRAINT `ordenlimpiezalab_ibfk_3` FOREIGN KEY (`idUserAsignado`) REFERENCES `usuario` (`idUser`),
  CONSTRAINT `ordenlimpiezalab_ibfk_4` FOREIGN KEY (`idTipoAsignacion`) REFERENCES `tipoasignacion` (`idTipoAsignacion`),
  CONSTRAINT `ordenlimpiezalab_ibfk_5` FOREIGN KEY (`idUserCreo`) REFERENCES `usuario` (`idUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*
  TABLA: historicoestado
*/
DROP TABLE IF EXISTS `historicoestado`;

CREATE TABLE `historicoestado` (
  `idhistorico` int(11) NOT NULL AUTO_INCREMENT,
  `fechaestado` datetime NOT NULL DEFAULT current_timestamp(),
  `idEstadoOrden` varchar(50) NOT NULL,
  `idOrden` int(11) NOT NULL,
  `idUser` varchar(80) NOT NULL,
  PRIMARY KEY (`idhistorico`),
  KEY `idEstadoOrden` (`idEstadoOrden`),
  KEY `idOrden` (`idOrden`),
  KEY `idUser` (`idUser`),
  CONSTRAINT `historicoestado_ibfk_1` FOREIGN KEY (`idEstadoOrden`) REFERENCES `estadoordenlimpieza` (`idEstadoOrden`),
  CONSTRAINT `historicoestado_ibfk_2` FOREIGN KEY (`idOrden`) REFERENCES `ordenlimpiezalab` (`idOrden`),
  CONSTRAINT `historicoestado_ibfk_3` FOREIGN KEY (`idUser`) REFERENCES `usuario` (`idUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `infofinordenlimpiezalab`;

CREATE TABLE `infofinordenlimpiezalab` (
  `idOrden` int(11) NOT NULL AUTO_INCREMENT,
  `idUser` varchar(80) NOT NULL,
  `multimedia` longblob DEFAULT NULL,
  `mimeType` varchar(64) DEFAULT NULL,
  `fechaIngreso` datetime NOT NULL,
  PRIMARY KEY (`idOrden`),
  KEY `idUser` (`idUser`),
  CONSTRAINT `infofinordenlimpiezalab_ibfk_1` FOREIGN KEY (`idOrden`) REFERENCES `ordenlimpiezalab` (`idOrden`),
  CONSTRAINT `infofinordenlimpiezalab_ibfk_2` FOREIGN KEY (`idUser`) REFERENCES `usuario` (`idUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `labusuarioaccesos`;

CREATE TABLE `labusuarioaccesos` (
  `idLab` int(11) NOT NULL,
  `idUser` varchar(80) NOT NULL,
  KEY `idLab` (`idLab`),
  KEY `idUser` (`idUser`),
  CONSTRAINT `labusuarioaccesos_ibfk_1` FOREIGN KEY (`idLab`) REFERENCES `laboratorio` (`idLab`),
  CONSTRAINT `labusuarioaccesos_ibfk_2` FOREIGN KEY (`idUser`) REFERENCES `usuario` (`idUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*
  PROCEDIMIENTOS ALMACENADOS
*/

DELIMITER ;;

CREATE  PROCEDURE `DeleteEdificio`(in idedificiop int)
IF EXISTS (SELECT * FROM edificio WHERE idedificio=idEdificiop)
THEN
 DELETE FROM edificio WHERE idedificio=idEdificiop;
 END IF ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `DeleteLaboratorio`(in idLab int)
IF EXISTS (SELECT * FROM laboratorio WHERE idLab=idLab)
THEN
 DELETE FROM laboratorio WHERE idLab=idLab;
 END IF ;;
DELIMITER ;

DELIMITER ;;
CREATE  PROCEDURE `DeleteUser`(in idCorreo varchar(100))
IF EXISTS (SELECT * FROM usuario WHERE idUser=idCorreo)
THEN
 DELETE FROM usuario WHERE idUser=idCorreo;
 END IF ;;
DELIMITER ;

DELIMITER ;;
CREATE PROCEDURE `InsertarUsers`(in idCorreo varchar(100),in nombres varchar(100), apellidos varchar(100), Edad int,
in Genero char,in Contra varchar(100),in fechaCreacion date,in idturno int, in idTipo int)
Begin
insert into usuario(idUser,nombre,apellido,edad,genero,pass,fechaCreacion,idTurno,idTipo)
 values (idCorreo,nombres,apellidos,Edad,Genero,md5(Contra),fechaCreacion,idturno,idTipo);
 
 END ;;
DELIMITER ;

CALL InsertarUsers('admin@utec.com', 'Jose Ernesto', 'Mejia', 27, 'M', 'admin', date(now()), 1, 1);
CALL InsertarUsers('encargado@utec.com', 'Julian', 'Garcia', 26, 'M', 'encargado', date(now()), 1, 2);
CALL InsertarUsers('ordenanza@utec.com', 'Manuel', 'Gomez', 25, 'M', 'ordenanza', date(now()), 1, 3);

DELIMITER ;;
CREATE  PROCEDURE `InsertEdificio`(Nombre varchar(100), Descripcion varchar(300))
BEGIN
insert into edificio (nombre,descripcion) values(Nombre,Descripcion);
END ;;
DELIMITER ;

DELIMITER ;;
CREATE  PROCEDURE `InsertLaboratorio`(in Nombre varchar(100), Descripcion varchar(300),idhorario int,idedificio int)
BEGIN
insert into laboratorio (nombre,descripcion,idhorario,idEdificio) values(Nombre,Descripcion,idhorario,idedificio);
END ;;
DELIMITER ;

DELIMITER ;;
CREATE  PROCEDURE `UpdateEdificio`(idedificiop int,Nombre varchar(100),Descripcion varchar(300))
BEGIN
update  edificio set nombre=Nombre,descripcion=Descripcion where idEdificio=idedificiop;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE  PROCEDURE `UpdateLaboratorio`(in lab int,in Nombre varchar(100), Descripcion varchar(300),idhorario int,idedificio int)
BEGIN
update  Laboratorio set idLab=lab,nombre=Nombre,descripcion=Descripcion,idHorario=idhorario,idEdificio=idEdificio
where idLab=Lab;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE procedure `UpdateUser`(idCorreo varchar(100),nombres varchar(100), apellidos varchar(100), Edad int,genero char(1),
Contra varchar(100),idturno int,idTipo int)
BEGIN
UPDATE usuario set  nombre=Nombres,apellido=apellidos,edad=Edad,genero=genero,pass=Contra,idTurno=idturno,idTipo=idTipo
where idUser=idCorreo;
END ;;
DELIMITER ;

DELIMITER //
create procedure buscar_order(
  _idOrden int,
  _fechaInicio datetime,
  _idEstadoOrden varchar(50),
  _idLab int(11),
  _idUserAsignado varchar(80),
  _idTipoAsignacion int(11),
  _idUserCreo varchar(80)
)
BEGIN
  select * from  ordenlimpiezalab as ol
  inner join usuario u on u.idUser=ol.idUserAsignado
  inner join estadoordenlimpieza el on el.idEstadoOrden=ol.idEstadoOrden
  inner join tipoasignacion ta on ta.idTipoAsignacion=ol.idTipoAsignacion
  inner join laboratorio la on la.idLab=ol.idLab
  where
  (_idOrden=0 or _idOrden=ol.idOrden ) OR
  (_fechaInicio=0 OR _fechaInicio=ol.fechaInicio  like   ('%'+ol.fechaProyectadaFin + '%') ) OR
  (_idEstadoOrden=0 OR _idEstadoOrden=el.nombre  like   (el.nombre + '%') ) OR
  (_idLab=0 OR _idLab=la.nombre like   (la.nombre + '%') ) OR
  (_idUserAsignado=0 OR _idUserAsignado=u.idUser like   (u.idUser + '%') ) OR
  (_idTipoAsignacion=0 OR _idTipoAsignacion=ta.Descripcion like   (la.Descripcion + '%'))  OR
  (_idUserCreo=0 OR _idUserCreo=u.idUser like   (u.idUser + '%')); 
END //

DELIMITER //
CREATE PROCEDURE `sp_detalle_Usu`(
  usu varchar(150)
)
BEGIN
  if exists(select * from usuario where idUser = usu) then 
      select idUser as Correo, concat(u.nombre,' ',u.apellido) as nombre,u.edad, u.genero, t.Descripcion as turno,tp.descripcion
    from usuario  as u inner join turno as t using(idTurno) 
    inner join tipousuario as tp using(idTipo) where idUser = usu ;
    end if;
END //

DELIMITER //
CREATE PROCEDURE ChangeContra (idusuario varchar(100),Contra varchar(100) )
BEGIN
IF EXISTS (SELECT * FROM usuario where idUser=idusuario)
THEN
update usuario set Pass=md5(Contra) where idUser=idusuario;
END IF;
END //

DELIMITER //
CREATE PROCEDURE Login (idUsuario varchar(100),Contra varchar(100))
BEGIN
IF EXISTS (select * from usuario u where idUser=idUsuario)
THEN
select * from usuario u
inner join tipousuario as tusuario on tusuario.idTipo=u.idTipo
where idUser=idUsuario AND Pass=md5(Contra);
END IF;
END //

DELIMITER //
CREATE PROCEDURE BuscarUser (idUsuario varchar(100), nom varchar(100), Apellido varchar(100), Edad int, gender char(1),
 fCreacion datetime,Tnombre varchar(100), Turno varchar(100))
BEGIN
select * from usuario u
inner join tipousuario as tusuario on tusuario.idTipo=u.idTipo 
inner join Turno as  t on t.idTurno=u.idTurno
where (idUsuario=0 or idUsuario=u.idUser like   (u.idUser + '%')) AND
(nom=0 or nom=u.nombre like (u.nombre + '%')) AND
(Apellido=0 or Apellido=u.apellido like (u.apellido + '%')) and
(Edad=0 or Edad=u.edad) AND
(gender=0 or gender=u.genero) and
(fCreacion=0 or fCreacion=u.fechaCreacion like (u.fechaCreacion + '%')) and
(Tnombre=0 or Tnombre=tusuario.nombre like (tusuario.nombre + '%')) and
(Turno=0 or Turno=t.Descripcion like (t.Descripcion + '%'));
END //

DELIMITER //
CREATE PROCEDURE CrearTipoAsignacion(Des varchar(100))
BEGIN
insert into tipoasignacion ( Descripcion) values(des);
END//

-- Cuadro o listview de las ordenes de limpieza
DELIMITER //
CREATE PROCEDURE `sp_Ordenes_Limpieza_Cuadro`(
  _mostrar varchar(10)
)
BEGIN
  select lim.idOrden as Orden_Trabajo, lab.nombre as Laboratorio, fechaCreacion as Fecha_Asignacion 
from infofinordenlimpiezalab as inf inner join ordenlimpiezalab as lim using(idOrden)
 inner join laboratorio as lab using(idLab) where idUser = _mostrar;
END //

-- boton ver 
DELIMITER //
CREATE  PROCEDURE `sp_BotonVer`(
  orden varchar(150)
)
BEGIN
  select lim.idOrden as Orden_Trabajo, fechaCreacion as Fecha_Asignacion,
  e.nombre, lab.nombre as Laboratorio, lim.fechaProyectadaFin as Tiempo_Estimado
  from infofinordenlimpiezalab as inf ,ordenlimpiezalab as lim inner join laboratorio as lab using(idLab)
  inner join edificio as e using(idEdificio) where lim.idOrden = orden or inf.idUser = orden;
END //

DELIMITER //
CREATE  PROCEDURE `InformacionContrasena`(
  usu varchar(150)
)
BEGIN
select u.idUser as correo, concat(u.nombre,' ',u.apellido) as nombre ,e.nombre
from ordenlimpiezalab as ol inner join usuario as u on u.idUser=ol.idUserAsignado inner join laboratorio using(idLab)
inner join edificio as e using(idEdificio) where u.idUser = usu;
END //

-- vista de los laboratorios asignados al ordenanza
DELIMITER //
CREATE PROCEDURE `LaboratorioAsignados`(
  usu varchar(150)
)
BEGIN
  select l.nombre ,l.descripcion
  from usuario as u inner join ordenlimpiezalab as ol on u.idUser = ol.idUserAsignado inner join laboratorio as l
  where u.idUser = usu;
END //

DELIMITER //
CREATE PROCEDURE `HistorialOrdenes` 
(
  usu varchar(150)
)
BEGIN
  select ol.idOrden as orden, l.nombre, ol.descripcion,ol.fechaCreacion 
  from usuario as u inner join ordenlimpiezalab as ol on u.idUser=ol.idUserAsignado inner join laboratorio as l using(idLab)
  where u.idUser = usu;
END // 

CREATE PROCEDURE `IntroducirEstados` 
(
  usu varchar(150),
    fecha datetime,
    estado varchar (10),
    orden int(10)
)
BEGIN
if exists (select * from historicoestado where idUser=usu) then 
  insert historicoestado(fechaestado,idEstadoOrden,idOrden,idUser) values (usu,fecha,estado,orden);
    END if;
END //

--  ver el trabajo ya se a finalizado o pausado 
DELIMITER // 
CREATE PROCEDURE `VerOdernTrabajo`
(
  usu varchar(150)
)
BEGIN
-- no se si saldra bien jajajja
select e.nombre,h.fechaestado, e.idEstadoOrden,ol.descripcion as comentario from ordenlimpiezalab as ol
inner join usuario as u on ol.idUserAsignado=u.idUser
inner join historicoestado as h on ol.idOrden=h.idOrden
inner join estadoordenlimpieza as e on ol.idEstadoOrden=e.idEstadoOrden
where u.idUser = usu ;
END //
delimiter //
CREATE  PROCEDURE `create_order`(
	_titulo varchar(200),
	_descripcion varchar(1000),
	_fechaInicio datetime,
	_fechaProyectadaFin datetime,
	_idEstadoOrden varchar(50),
	_idLab int(11),
	_idUserAsignado varchar(80),
	_idTipoAsignacion int(11),
	_idUserCreo varchar(80)
)
begin
 insert into ordenlimpiezalab (titulo,descripcion,fechaCreacion,fechaInicio,fechaProyectadaFin,idEstadoOrden,idLab,idUserAsignado,idTipoAsignacion,idUserCreo)
 values (_titulo,_descripcion,now(),_fechaInicio,_fechaProyectadaFin,_idEstadoOrden,_idLab,_idUserAsignado,_idTipoAsignacion,_idUserCreo);
end//
delimiter ;

delimiter //
CREATE  PROCEDURE `filtro_encargado_historial`(
	in _descripcion varchar(90),
    in _estado varchar(200),
    in _fecha datetime)
BEGIN
	
  select lab.idLab as Laboratorio, edi.nombre as Edificio, concat(usu.nombre,' ',usu.apellido) as Ordenanza,
	date(orden.fechaInicioProgreso) as Dia, time(orden.fechaInicio) as Hora_Inicial, time(orden.fechaFinalizo) as Hora_Final,
	estado.nombre as Estado from ordenlimpiezalab as orden inner join laboratorio as lab on
	orden.idLab = lab.idLab inner join edificio as edi on lab.idEdificio = edi.idEdificio
	inner join usuario as usu on orden.idUserAsignado = usu.idUser inner join estadoordenlimpieza as estado
	on orden.idEstadoOrden = estado.idEstadoOrden inner join tipoasignacion as tipo on
	orden.idTipoAsignacion = tipo.idTipoAsignacion
	where usu.idTipo =4 and tipo.Descripcion =_descripcion and estado.nombre =_estado and orden.fechaInicioProgreso =_fecha;
	

END//

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `CambiarEstado`(id varchar(100),estado varchar(100))
BEGIN
IF EXISTS (SELECT * FROM ordenlimpiezalab where idOrden=id)
THEN
/* cambiar estado */
update ordenlimpiezalab set idEstadoOrden=estado where idOrden=id;
/*cambiar hora del estado */
update ordenlimpiezalab set fechaInicioProgreso=now() where idOrden=id;
/* llenar historico */
INSERT INTO historicoestado (fechaestado, idEstadoOrden,idOrden,idUser) SELECT fechaInicioProgreso, idEstadoOrden,idOrden,idUserCreo from ordenlimpiezalab where idOrden=id;
END IF;
END$$

delimiter ;

DROP TABLE IF EXISTS `asignacionlab`;

CREATE TABLE `asignacionlab` (
  `idAsignacionLab` int(11) NOT NULL AUTO_INCREMENT,
  `idencargado` varchar(100) DEFAULT NULL,
  `idLabo` int(11) DEFAULT NULL,
  PRIMARY KEY (`idAsignacionLab`),
  KEY `idencargado` (`idencargado`),
  KEY `idLabo` (`idLabo`),
  CONSTRAINT `asignacionlab_ibfk_1` FOREIGN KEY (`idencargado`) REFERENCES `usuario` (`idUser`),
  CONSTRAINT `asignacionlab_ibfk_2` FOREIGN KEY (`idLabo`) REFERENCES `laboratorio` (`idLab`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;