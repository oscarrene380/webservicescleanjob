/*
sp de insertar ordenes de limpieza

*validaciones*

0- todo correcto y se inserta la orden de limpieza
1- El laboratorio al que se esta asignando la orden no coincide con el asignado al usuario
2- ya existen asignaciones en la fechas indicadas para este rango de fechas para este usuario
3- el usuario tiene turno de la mañana pero se le definio un horario de la tarde o fuera de su rango en la fecha que no corresponde a su turno 
   manaña 06:00:00 - 12:00:00 (este es el formato de la hora que debe venir en la fecha)
   tarde 12:01:00 - 20:00:00
4-el usuario tiene turno de la tarde pero se le definio un horario de la mañana o fuera de su rango en la fecha que no corresponde a su turno 
ejemplo de fecha ('2020-12-28 10:00:00') 10am
('2020-12-28 17:00:00')  5pm

****adicionales-****
codigos de tiposasignacion segun el sp
1-dia
2-mes
3-semana

codigos de turnos segun el sp
1-turno mañana
2- turno tarde

*/
 DELIMITER //
create PROCEDURE InsertOrdenLimpieza(
  _idUserAsignado varchar(80),
  _idUser varchar(80),
  _FechaAsignado DateTime,
  _idTipoAsig int,
  _titulo varchar(200),
  _Descripcion varchar(200),
  _idlab int
)
BEGIN
declare respuesta int default 0;
declare tiempo time;

declare turnomanana int default 1;
declare turnotarde int default 2;

declare tipoDia int default 1;
declare tipoMes int default 2;
declare tipoSemana int default 3;

declare idTurnoUser int(11) default 0 ;
declare idlabuser int(11);
declare _fechaFin  Datetime ;
declare existeAsignacion int default 0;

set tiempo =  CAST(DATE_FORMAT(_FechaAsignado,'%H:%i:%s') as time);

set idTurnoUser=(select idTurno from usuario where idUser=_idUserAsignado);
set idlabuser=(select idLab from usuario where idUser=_idUserAsignado);

if(_idTipoAsig=tipoDia)then
set _fechaFin= (select DATE_ADD(_FechaAsignado, INTERVAL 1 DAY)); 
elseif(_idTipoAsig=tipoMes)then
set _fechaFin= (select DATE_ADD(_FechaAsignado, INTERVAL 30 DAY));
else
set _fechaFin= (select DATE_ADD(_FechaAsignado, INTERVAL 7 DAY));
end if;
set existeAsignacion=(select Count(idUserAsignado) from ordenlimpiezalab
where ((_FechaAsignado >= fechaInicio and _fechaFin <= fechaProyectadaFin)
or (_FechaAsignado <= fechaInicio and _fechaFin >= fechaProyectadaFin)
or (_FechaAsignado <= fechaInicio and (_fechaFin >=fechaInicio  and _fechaFin <= fechaProyectadaFin))
or (_fechaFin >= fechaProyectadaFin and (_FechaAsignado <= fechaProyectadaFin  and _FechaAsignado >= fechaInicio))) and idUserAsignado=_idUserAsignado) ;

if(_idlab !=idlabuser)then
set respuesta  = 1;
elseif (existeAsignacion > 0)
then
set respuesta  = 2;
elseif idTurnoUser = turnomanana and  (tiempo > (select CAST('12:01:00' AS time)) or tiempo < (select CAST('06:00:00' AS time)))
then
   set respuesta  = 3;
elseif idTurnoUser = turnotarde and  (tiempo > (select CAST('20:01:00' AS time)) or tiempo < (select CAST('12:00:00' AS time)))
 then
  set respuesta  = 4;
else
 insert into ordenlimpiezalab(
titulo,
descripcion,
fechaInicio,
fechaProyectadaFin,
idEstadoOrden,
idLab,
idUserAsignado,
idTipoAsignacion,
idUserCreo,
fechaCreacion
) values(_titulo,_Descripcion,_FechaAsignado,_fechaFin,'PEN',_idlab,_idUserAsignado,_idTipoAsig,_idUser,now());
end if;

 select respuesta as respuesta ;
END 
// DELIMITER ;