<?php
require_once('conectar.php');
require_once dirname(dirname(__FILE__))."/phpmailer/enviar.php";
class CuentaSQL
{
	public function getconexion()
	{
		$conectarnos = new conectar();
       	return $conectarnos;		
	}

	public function Login($user, $password)
	{
		$procedimiento = $this->getconexion()->prepare("CALL Login(:user,:password)");
		$procedimiento->bindParam(":user",$user);	
		$procedimiento->bindParam(":password",$password);
		$procedimiento->execute();
		$result = $procedimiento->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	public function ResetPassword($user, $newPassword)
	{
		$procedimiento = $this->getconexion()->prepare("CALL ChangeContra(:user,:password)");
		$procedimiento->bindParam(":user",$user);	
		$procedimiento->bindParam(":password",$newPassword);
		$procedimiento->execute();
		$actualizado = $procedimiento->rowCount();
		if($actualizado == 1)
		{
			$enviar = new Email;
			$enviar->sendMail("envio", $user, "Cambio de contraseña", 2, "lorem");
		}
		return $actualizado;
	}

	public function RecuperarCuenta($user)
	{
		$newPassword = $this->generaClave();
		$procedimiento = $this->getconexion()->prepare("CALL ChangeContra(:user,:password)");
		$procedimiento->bindParam(":user",$user);	
		$procedimiento->bindParam(":password",$newPassword);
		$procedimiento->execute();
		$actualizado = $procedimiento->rowCount();
		if($actualizado == 1)
		{
			$enviar = new Email;
			$enviar->sendMail("envio", $user, "Recuperacion de cuenta", 3, $newPassword);
		}
		return $actualizado;
	}

	private function generaClave()
	{
	    //Creamos la contraseña        
	    $cadena = "ABCDFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
	    $longitudCadena = strlen($cadena);
	    $pass = "";
	    $longitudPass = 11;
	    for($i=1 ; $i<=$longitudPass ; $i++){
	        //Definimos numero aleatorio entre 0 y la longitud de la cadena de caracteres-1
	        $pos=rand(0,$longitudCadena-1);
	     
	        //Vamos formando la contraseña en cada iteraccion del bucle, añadiendo a la cadena $pass la letra correspondiente a la posicion $pos en la cadena de caracteres definida.
	        $pass .= substr($cadena,$pos,1);
	    }
	    return $pass;
	}
}