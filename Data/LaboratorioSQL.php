<?php
require_once "conectar.php";
class LaboratorioSQL
{
	public function getconexion()
	{
		$conectarnos = new conectar();
       	return $conectarnos;		
	}

	public function InsertLaboratorio($nombre,$descripcion,$idEdificio)
	{
		$procedimiento = $this->getconexion()->prepare('CALL InsertLaboratorio(:nombre,:descripcion,:idEdificio)');
		$procedimiento->bindParam(':nombre',$nombre);
		$procedimiento->bindParam(':descripcion',$descripcion);
		$procedimiento->bindParam(':idEdificio',$idEdificio);

		$procedimiento->execute();
		$insertado = $procedimiento->rowCount();
		return $insertado;
	}

	public function UpdateLaboratorio($lab,$nombre,$descripcion,$edificio)
	{
		$procedimiento = $this->getconexion()->prepare('CALL UpdateLaboratorio (:lab,:nombre,:descripcion,:edificio)');
		$procedimiento->bindParam(':lab',$lab);
		$procedimiento->bindParam(':nombre',$nombre);
		$procedimiento->bindParam(':descripcion',$descripcion);
		$procedimiento->bindParam(':edificio',$edificio);

		$procedimiento->execute();
		$actualizado->procedimiento->rowCount();
		return $actualizado;
	}

	public function DeleteLaboratorio($idLab)
	{
		$procedimiento = $this->getconexion()->prepare('CALL DeleteLaboratorio (:idLaboratorio)');
		$procedimiento->bindParam(':idLaboratorio',$idLaboratorio);

		$procedimiento->execute();
		$eliminado = $procedimiento->rowCount();
		return $eliminado;
	}

	public function ListaLaboratorios()
	{
		$procedimiento = $this->getconexion()->prepare("SELECT l.idLab,l.nombre as laboratorio,e.nombre as edificio,e.descripcion FROM laboratorio l join edificio e using(idEdificio)");
		$procedimiento->execute();
		$result = $procedimiento->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	public function DetalleLab($buscar)
	{
		$procedimiento = $this->getconexion()->prepare("SELECT l.idLab,l.nombre as laboratorio,e.nombre as edificio,e.descripcion FROM laboratorio l join edificio e using(idEdificio) WHERE l.idLab = :id");
		$procedimiento->bindParam(":id",$buscar);
		$procedimiento->execute();
		$result = $procedimiento->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}
}