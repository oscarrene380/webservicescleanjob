<?php
require_once('conectar.php');
require_once dirname(dirname(__FILE__))."/phpmailer/enviar.php";
class UsuarioSQL
{
	public function getconexion()
	{
		$conectarnos = new conectar();
       	return $conectarnos;	
	}
	
	public function InsertUsers($idUser,$nombre,$apellido,$edad,$genero,$idTurno,$idTipo,$idLab)
	{
		$pass = $this->generaClave();
	    $fechaActual = date("Y/m/d");
		$procedimiento =$this->getconexion()->prepare('Call InsertarUsers(:idUser,:nombre,:apellido,:edad,:genero,:pass,:fechaCreacion,:idTurno,:idTipo,:idLab)');
		$procedimiento->bindParam(':idUser',$idUser);
		$procedimiento->bindParam(':nombre',$nombre);
		$procedimiento->bindParam(':apellido',$apellido);
		$procedimiento->bindParam(':edad',$edad);
		$procedimiento->bindParam(':genero',$genero);
		$procedimiento->bindParam(':pass',$pass);
		$procedimiento->bindParam(':fechaCreacion',$fechaActual);
		$procedimiento->bindParam(':idTurno',$idTurno);
		$procedimiento->bindParam(':idTipo',$idTipo);
		$procedimiento->bindParam(':idLab',$idLab);
		$procedimiento->execute();
		$insertado=$procedimiento->rowCount();
		if($insertado == 1)
		{
			$enviar = new Email;
			$enviar->sendMail($nombre, $idUser, "Credenciales de acceso", 1, $pass);
		}
		return $insertado;
	}

	public function UpdateUser($idUser,$nombre,$apellido,$edad,$genero,$idTurno,$idTipo,$idLab)
	{
		$procedimiento =$this->getconexion()->prepare('Call UpdateUser(:idUser,:nombre,:apellido,:edad,:genero,:idTurno,:idTipo,:idLab)');
		$procedimiento->bindParam(':idUser',$idUser);
		$procedimiento->bindParam(':nombre',$nombre);
		$procedimiento->bindParam(':apellido',$apellido);
		$procedimiento->bindParam(':edad',$edad);
		$procedimiento->bindParam(':genero',$genero);
		$procedimiento->bindParam(':idTurno',$idTurno);
		$procedimiento->bindParam(':idTipo',$idTipo);
		$procedimiento->bindParam(':idLab',$idLab);
		$procedimiento->execute();
		$actualizado=$procedimiento->rowCount();
		return $actualizado;
	}
	
	public function DeleteUsers($iduser)
	{
		$procedimiento =$this->getconexion()->prepare('Call DeleteUser(:iduser)');
		$procedimiento->bindParam(':iduser',$iduser);
		$procedimiento->execute();
		$eliminado=$procedimiento->rowCount();
		return $eliminado;
	}

	public function ListaUsuarios()
	{
		$sql = "SELECT  idUser,u.nombre,apellido,edad,genero,t.idTurno,t.Descripcion turno,tu.idTipo,tu.nombre tipo ,l.idLab,l.nombre laboratorio,Activo
				from usuario u join turno t using(idTurno) join tipousuario tu using(idTipo) join laboratorio l group by idUser";
		$procedimiento = $this->getconexion()->prepare($sql);
		$procedimiento->execute();
		$result = $procedimiento->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	public function DetalleUser($buscar)
	{
		$sql = "SELECT idUser,u.nombre,apellido,edad,genero,t.idTurno,t.Descripcion turno,tu.idTipo,tu.nombre tipo ,l.idLab,l.nombre laboratorio,Activo
				from usuario u join turno t using(idTurno) join tipousuario tu using(idTipo) join laboratorio l WHERE  idUser = '".$buscar."' group by idUser";
		$procedimiento = $this->getconexion()->prepare($sql);
		$procedimiento->execute();
		$result = array($procedimiento->fetch(PDO::FETCH_ASSOC));
		return $result;
	}

	private function generaClave()
	{
	    //Creamos la contraseña        
	    $cadena = "ABCDFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
	    $longitudCadena = strlen($cadena);
	    $pass = "";
	    $longitudPass = 11;
	    for($i=1 ; $i<=$longitudPass ; $i++){
	        //Definimos numero aleatorio entre 0 y la longitud de la cadena de caracteres-1
	        $pos=rand(0,$longitudCadena-1);
	     
	        //Vamos formando la contraseña en cada iteraccion del bucle, añadiendo a la cadena $pass la letra correspondiente a la posicion $pos en la cadena de caracteres definida.
	        $pass .= substr($cadena,$pos,1);
	    }
	    return $pass;
	}
}
