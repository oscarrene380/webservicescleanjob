<?php
require_once "conectar.php";
class AsignacionesSQL
{
	public function getconexion()
	{
		$conectarnos = new conectar();
       	return $conectarnos;		
	}

	public function CrearAsignacion($idUserAsignado,$idUser,$FechaAsignado,$idTipoAsig,$titulo,$Descripcion,$idlab)
	{
		$procedimiento = $this->getconexion()->prepare("CALL InsertOrdenLimpieza (:idUserAsignado,:idUser,:FechaAsignado,:idTipoAsig,:titulo,:Descripcion,:idlab)");
		$procedimiento->bindParam(":idUserAsignado",$idUserAsignado);
		$procedimiento->bindParam(":idUser",$idUser);
		$procedimiento->bindParam(":FechaAsignado",$FechaAsignado);
		$procedimiento->bindParam(":idTipoAsig",$idTipoAsig);
		$procedimiento->bindParam(":titulo",$titulo);
		$procedimiento->bindParam(":Descripcion",$Descripcion);
		$procedimiento->bindParam(":idlab",$idlab);
		$procedimiento->execute();
		$result = $procedimiento->fetchAll(PDO::FETCH_ASSOC);
		return $result['respuesta'];
	}

	public function BuscarOrder($idOrden,$fechaInicio,$idEstadoOrden,$idLab,$idUserAsignado,$idTipoAsignacion,$idUserCreo)
	{
		$procedimiento = $this->getconexion()->prepare("CALL buscar_order(:idOrden,:fechaInicio,:idEstadoOrden,:idLab,:idUserAsignado,:idTipoAsignacion,:idUserCreo)");
		$procedimiento->bindParam(":idOrden",$idOrden);
		$procedimiento->bindParam(":fechaInicio",$fechaInicio);
		$procedimiento->bindParam(":idEstadoOrden",$idEstadoOrden);
		$procedimiento->bindParam(":idLab",$idLab);
		$procedimiento->bindParam(":idUserAsignado",$idUserAsignado);
		$procedimiento->bindParam(":idTipoAsignacion",$idTipoAsignacion);
		$procedimiento->bindParam(":idUserCreo",$idUserCreo);
		$procedimiento->execute();
		$result = $procedimiento->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	public function ListarOrdenes($idlab,$idUserAsignado,$idsEstados,$idUsercreo)
	{
		$procedimiento = $this->getconexion()->prepare("CALL SpListadoOrdenesLimpiezaFiltradas(:idlab,:idUserAsignado,:idsEstados,:idUsercreo)");
		$procedimiento->bindParam(":idlab",$idlab);
		$procedimiento->bindParam(":idUserAsignado",$idUserAsignado);
		$procedimiento->bindParam(":idsEstados",$idsEstados);
		$procedimiento->bindParam(":idUsercreo",$idUsercreo);
		$procedimiento->execute();
		$result = $procedimiento->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}
}