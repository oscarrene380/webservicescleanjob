<?php
require_once('../Data/LaboratorioSQL.php');
class LaboratorioService
{
	public function InsertLaboratorio($nombre,$descripcion,$idEdificio)
	{
		$LabSQL = new LaboratorioSQL(); 
		$rawdata = $LabSQL->InsertLaboratorio($nombre,$descripcion,$idEdificio);
		return ($rawdata);
	}

	public function UpdateLaboratorio($lab,$nombre,$descripcion,$edificio)
	{
		$LabSQL = new LaboratorioSQL();
		$rawdata = $LabSQL->UpdateLaboratorio($lab,$nombre,$descripcion,$edificio);
		return ($rawdata);
	}

	public function DeleteLaboratorio($idLab)
	{
		$LabSQL = new LaboratorioSQL();
		$rawdata = $LabSQL->DeleteLaboratorio($idLab);
		return ($rawdata);
	}

	public function ListaLaboratorios()
	{
		$LabSQL = new LaboratorioSQL();
		$rawdata = $LabSQL->ListaLaboratorios();
		return ($rawdata);
	}

	public function DetalleLab($buscar)
	{
		$LabSQL = new LaboratorioSQL();
		$rawdata = $LabSQL->DetalleLab($buscar);
		return ($rawdata);
	}
}
	