<?php
require_once('../Data/UsuarioSQL.php');
class UsuarioService
{
	public function InsertUsers($idUser,$nombre,$apellido,$edad,$genero,$idTurno,$idTipo,$idLab)
	{
		$UserSQL = new UsuarioSQL(); 
		$rawdata=$UserSQL->InsertUsers($idUser,$nombre,$apellido,$edad,$genero,$idTurno,$idTipo,$idLab);
		return ($rawdata);
	}
	
	public function UpdateUser($idUser,$nombre,$apellido,$edad,$genero,$idTurno,$idTipo,$idLab)
	{
		$UserSQL = new UsuarioSQL(); 
		$rawdata=$UserSQL->UpdateUser($idUser,$nombre,$apellido,$edad,$genero,$idTurno,$idTipo,$idLab);
		return ($rawdata);
	}
	
	public function DeleteUsers($idUser)
	{
		$UserSQL = new UsuarioSQL(); 
		$rawdata=$UserSQL->DeleteUsers($idUser);
		return ($rawdata);	
	}
	
	public function ListaUsuarios()
	{
		$UserSQL = new UsuarioSQL();
		$rawdata = $UserSQL->ListaUsuarios();
		return ($rawdata);
	}

	public function DetalleUser($buscar)
	{
		$UserSQL = new UsuarioSQL();
		$rawdata = $UserSQL->DetalleUser($buscar);
		return ($rawdata);
	}
}