<?php
require_once "../Data/AsignacionesSQL.php";
class AsignacionesService
{
	public function CrearAsignacion($idUserAsignado,$idUser,$FechaAsignado,$idTipoAsig,$titulo,$Descripcion,$idlab)
	{
		$AsignacionesSQL = new AsignacionesSQL();
		$rawData = $AsignacionesSQL->CrearAsignacion($idUserAsignado,$idUser,$FechaAsignado,$idTipoAsig,$titulo,$Descripcion,$idlab);
		return ($rawData);
	}

	public function BuscarOrder($idOrden,$fechaInicio,$idEstadoOrden,$idLab,$idUserAsignado,$idTipoAsignacion,$idUserCreo)
	{
		$AsignacionesSQL = new AsignacionesSQL();
		$rawData = $AsignacionesSQL->BuscarOrder($idOrden,$fechaInicio,$idEstadoOrden,$idLab,$idUserAsignado,$idTipoAsignacion,$idUserCreo);
		return ($rawData);
	}

	public function ListarOrdenes($idlab,$idUserAsignado,$idsEstados,$idUsercreo)
	{
		$AsignacionesSQL = new AsignacionesSQL();
		$rawData = $AsignacionesSQL->ListarOrdenes($idlab,$idUserAsignado,$idsEstados,$idUsercreo);
		return ($rawData);
	}
}