<?php
require_once "../Data/CuentaSQL.php";
class CuentaService
{
	public function Login($user, $password)
	{
		$CuentaSQL = new CuentaSQL(); 
		$rawdata = $CuentaSQL->Login($user,$password);
		return ($rawdata);
	}

	public function ResetPassword($user, $newPassword)
	{
		$CuentaSQL = new CuentaSQL();
		$rawdata = $CuentaSQL->ResetPassword($user,$newPassword);
		return ($rawdata);
	}

	public function RecuperarCuenta($user)
	{
		$CuentaSQL = new CuentaSQL();
		$rawdata = $CuentaSQL->RecuperarCuenta($user);
		return ($rawdata);
	}
}